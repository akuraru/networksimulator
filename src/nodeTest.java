import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import junit.framework.*;
import static org.hamcrest.CoreMatchers.*;

public class nodeTest {
	static int TRANS_RATE = 2000000;
	Node n;
	Packet p1 = new Packet(1, "", 512, 0);
	Packet p2 = new Packet(2, "", 512, 0);
	Packet p3Delay = new Packet(3, "", 512, 0.000256);
	Packet p4DelayMore = new Packet(1, "", 512, 0.000257);
	Packet p5 = new Packet(2, "", 512, 0.000257);

	@Before
	public void setup() {
		n = N.empty(TRANS_RATE, 0.2);
	}

	// reception Test
	@Test
	public void receptionパッケトTest() {
		NPPack np = N.reception(n, p1);
		Packet p = np.packet();
		assertEquals(p.id(), p1.id());
		assertEquals(p.message(), p1.message());
		assertEquals(p.size(), p1.size());
		double p1tr = P.transrateTime(p1, TRANS_RATE);
		double pst = p.startTime();
		assertEquals(p1tr <= pst && pst <= p1tr + 0.2, true);
	}

	@Test
	public void receptionノードバッファーTest() {
		NPPack np = N.reception(n, p1);
		Node n = np.node();
		Packet p = np.packet();
		assertEquals(N._inCList(n.Buffer(), p), true);
	}

	@Test
	public void receptionノードバッファー2Test() {
		NPPack np = N.reception(n, p1);
		Node n = np.node();
		Packet p = np.packet();
		assertEquals(N._inCList(n.Buffer(), p1), false);
	}

	@Test
	public void receptionノード受信リストTest() {
		NPPack np = N.reception(n, p1);
		Node n = np.node();
		assertEquals(N._inCList(n.RecepedList(), p1), true);
	}

	@Test
	public void receptionノード受信リスト2Test() {
		NPPack np = N.reception(n, p1);
		Node n = np.node();
		Packet p = np.packet();
		assertEquals(N._inCList(n.RecepedList(), p), false);
	}

	// canStartCommunicationTest
	@Test
	public void canStartCommunicationtest1() {
		NPPack np = N.reception(n, p1);
		Packet p = np.packet();
		Node node = np.node();
		assertEquals(N.canStartCommunication(node, p), true);
	}

	@Test
	public void canStartCommunicationtest2() {
		NPPack np = N.reception(n, p1);
		Packet p = np.packet();
		Node node = np.node();
		assertEquals(N.canStartCommunication(node, p1), false);
	}

	// 衝突した場合の処理
	@Test
	public void s衝突した場合_衝突されたPacketを削除する() {
		NPPack np1 = N.reception(n, p1);
		Packet p = np1.packet();
		NPPack np2 = N.reception(np1.node(), p2);
		assertEquals(N.canStartCommunication(np2.node(), p), false);
	}

	@Test
	public void 衝突した場合_衝突したpacketの送信はできない() {
		NPPack np1 = N.reception(n, p1);
		NPPack np2 = N.reception(np1.node(), p2);
		Packet p = np2.packet();
		Node node = np2.node();
		assertEquals(N.canStartCommunication(node, p), false);
	}

	@Test
	public void s衝突した場合でも受信パケットは保持する() {
		NPPack np1 = N.reception(n, p1);
		NPPack np2 = N.reception(np1.node(), p2);
		Node node = np2.node();
		assertEquals(N._inCList(node.RecepedList(), p1), true);
		assertEquals(N._inCList(node.RecepedList(), p2), true);
	}

	@Test
	public void 衝突しても関係ないものには影響はない() {
		NPPack np1 = N.reception(n, p1);
		Packet p = np1.packet();
		NPPack np2 = N.reception(np1.node(), p4DelayMore);
		NPPack np3 = N.reception(np2.node(), p5);
		assertEquals(N.canStartCommunication(np3.node(), p), true);
	}

	// 衝突判定
	@Test
	public void nowReceptionTest() {
		Boolean b = N.nowReceping(p1, p2, TRANS_RATE);
		assertEquals(b, true);
	}

	@Test
	public void nowReceptionTest2() {
		Boolean b = N.nowReceping(p1, p3Delay, TRANS_RATE);
		assertEquals(b, true);
	}

	@Test
	public void nowReceptionTest3() {
		Boolean b = N.nowReceping(p1, p4DelayMore, TRANS_RATE);
		assertEquals(b, false);
	}

	@Test
	public void TransrateTimeTest() {
		assertThat(P.transrateTime(p1, n.Rate()), is(0.000256));
	}
}
