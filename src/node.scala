case class NPPack(node: Node, packet: Packet)
case class Node(Rate: Double, RAT: Double = 0.2, Buffer: List[cPacket], RecepedList: List[cPacket])
case class cPacket(packet:Packet,id:Int)

import scala.util.Random

object N {
  def empty(Rate: Double, RAT: Double): Node = { new Node(Rate, RAT, List[cPacket](), List[cPacket]()) }
  def canStartCommunication(node: Node, packet: Packet): Boolean = {
    _inList(node.Buffer.map(_.packet), packet)
  }
  def _occur(node: Node, packet: Packet): (Node, Packet) = { (node, packet) }
  def occur(node: Node, packet: Packet): NPPack = {
    val (n, p) = _occur(node, packet)
    return new NPPack(n, p)
  }
  def _reception(node: Node, packet: Packet): (Node, Packet) = {
    if (node.RecepedList.map(_.packet).exists(p => nowReceping(p, packet, node.Rate))) {
      val l = node.RecepedList.filter(p => nowReceping(p.packet, packet, node.Rate))
      val b = node.Buffer.filter(p => !(l.exists(_.id == p.id)))
      (new Node(node.Rate, node.RAT, b, cPacket(packet, -1) :: node.RecepedList), packet)
    } else {
      val p = P.packetOfDelaied(packet, P.transrateTime(packet, node.Rate), node.RAT)
      val id = Random.nextInt(Integer.MAX_VALUE);
      (new Node(node.Rate, node.RAT, new cPacket(p, id) :: node.Buffer, new cPacket(packet, id) :: node.RecepedList), p)
    }
  }
  def reception(node: Node, packet: Packet): NPPack = {
    val (n, p) = _reception(node, packet)
    return new NPPack(n, p)
  }
  def nowReceping(packet1: Packet, packet2: Packet, rate:Double): Boolean = {
    val s1 = packet1.startTime
    val t1 = P.transrateTime(packet1, rate)
    val s2 = packet2.startTime
    val t2 = P.transrateTime(packet2, rate)
    (s1 <= s2 && s2 <= t1) || (s2 <= s1 && s1 <= t2)
  }

  def _inList(list: List[Packet], packet: Packet): Boolean = {
    list.exists(packet ==)
  }
  def _inCList(list: List[cPacket], packet: Packet): Boolean = {
    list.map(_.packet).exists(packet ==)
  }
}

case class Packet(id: Int, message: String = "", size: Int, startTime: Double)
object P {
  def transrateTime(packet: Packet, rate: Double): Double = { packet.startTime + packet.size / rate }
  def packetOfDelaied(packet: Packet, setTime: Double, maxDelayTime: Double): Packet = {
    new Packet(packet.id, packet.message, packet.size, setTime + Random.nextDouble() * maxDelayTime)
  }
}