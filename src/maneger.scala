class manager(c:Pattern) {
  def g (x:Int) = {
    1
  }
	
}

import scala.util.Random

class Pattern(){
  val interval:Double = 1.0;
  val nodeNum = 9;
  var n = -1;
  
  def createPacketFromNode():(Packet, Int) = {
    n += 1
    (new Packet(n, "", 512, n*interval), Random.nextInt(nodeNum))
  }
}